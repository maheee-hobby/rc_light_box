#include "RCInput.h"

#include "math.h"


RCInput::RCInput(uint8_t pinInputDir, uint8_t pinInputSteer) {
  this->pinInputDir = pinInputDir;
  this->pinInputSteer = pinInputSteer;

  driveInputValue = 1500;
  steerInputValue = 1500;
}

int RCInput::readPwm(uint8_t pin) {
  unsigned long start_time;
  unsigned long end_time;

  while (digitalRead(pin));
  while (!digitalRead(pin));

  start_time = micros();
  while (digitalRead(pin));
  end_time = micros();

  if (end_time > start_time) {
    return (int)(end_time - start_time);
  } else {
    return -1;
  }
}

void RCInput::update() {
  // Read RC Values
  newDriveInputValue = readPwm(pinInputDir);
  newSteerInputValue = readPwm(pinInputSteer);

  if (newDriveInputValue   != -1) { driveInputValue   = restrict(newDriveInputValue,    MIN_INPUT_VALUE, MAX_INPUT_VALUE); }
  if (newSteerInputValue != -1) { steerInputValue = restrict(newSteerInputValue,  MIN_INPUT_VALUE, MAX_INPUT_VALUE); }

  // Calculate RC Values (Dir)
  if (driveInputValue < DIR_LOW_THRESHOLD) {
    driveDir = -1;
    driveVal = ((DIR_LOW_THRESHOLD - driveInputValue) * 100) / (DIR_LOW_THRESHOLD - MIN_INPUT_VALUE);
  } else if (driveInputValue > DIR_HIGH_THRESHOLD) {
    driveDir = 1;
    driveVal = ((driveInputValue - DIR_HIGH_THRESHOLD) * 100) / (MAX_INPUT_VALUE - DIR_HIGH_THRESHOLD);
  } else {
    driveDir = 0;
    driveVal = 0;
  }
  // Calculate RC Values (Steer)
  if (steerInputValue < STEER_LOW_THRESHOLD) {
    steerDir = -1;
    steerVal = ((STEER_LOW_THRESHOLD - steerInputValue) * 100) / (STEER_LOW_THRESHOLD - MIN_INPUT_VALUE);
  } else if (steerInputValue > STEER_HIGH_THRESHOLD) {
    steerDir = 1;
    steerVal = ((steerInputValue - STEER_HIGH_THRESHOLD) * 100) / (MAX_INPUT_VALUE - STEER_HIGH_THRESHOLD);
  } else {
    steerDir = 0;
    steerVal = 0;
  }
}
