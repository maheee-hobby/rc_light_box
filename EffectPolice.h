#ifndef EffectPolice_H
#define EffectPolice_H


#include <Adafruit_NeoPixel.h>

#include "RCInput.h"
#include "Blink.h"


class EffectPolice {

  private:
    Blink *blink;
    bool lastBlink;

    int effectPosition;
    int flash;

    uint32_t colorRed;
    uint32_t colorBlue;
    uint32_t colorBlack;

  public:
    EffectPolice();
    void update(RCInput *rcInput, Adafruit_NeoPixel *pixelsFront, Adafruit_NeoPixel *pixelsBack);
};


#endif
