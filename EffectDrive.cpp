#include "EffectDrive.h"


EffectDrive::EffectDrive() {
  blink = new Blink(BLINK_INTERVAL);
}

void EffectDrive::update(RCInput *rcInput, Adafruit_NeoPixel *pixelsFront, Adafruit_NeoPixel *pixelsBack) {
  blink->update();

  int brightness = 100 + (rcInput->driveVal * 1.5);
  int lowBrightness = 100 + (rcInput->driveVal * 0.5);

  white     = pixelsFront->Color(lowBrightness, lowBrightness, lowBrightness);
  whiteHigh = pixelsFront->Color(brightness, brightness, brightness);
  red       = pixelsFront->Color(lowBrightness, 0, 0);
  redHigh   = pixelsFront->Color(brightness, 0, 0);

  whiteUltraLow = pixelsFront->Color(20, 20, 20);
  whiteLow      = pixelsFront->Color(30, 30, 30);
  redUltraLow   = pixelsFront->Color(20, 0, 0);
  redLow        = pixelsFront->Color(30, 0, 0);
  orange        = pixelsFront->Color(255, 80, 0);
  black         = pixelsFront->Color(0, 0, 0);

  // check driving direction
  if (rcInput->driveDir == 1) {
    // forward
    pixelsFront->setPixelColor(0, white);
    pixelsFront->setPixelColor(1, whiteHigh);
    pixelsFront->setPixelColor(2, whiteHigh);
    pixelsFront->setPixelColor(3, whiteHigh);
    pixelsFront->setPixelColor(4, white);
    
    pixelsBack->setPixelColor(0, red);
    pixelsBack->setPixelColor(1, redHigh);
    pixelsBack->setPixelColor(2, redHigh);
    pixelsBack->setPixelColor(3, redHigh);
    pixelsBack->setPixelColor(4, red);

  } else if (rcInput->driveDir == -1) {
    // backward
    pixelsFront->setPixelColor(0, redHigh);
    pixelsFront->setPixelColor(1, white);
    pixelsFront->setPixelColor(2, white);
    pixelsFront->setPixelColor(3, white);
    pixelsFront->setPixelColor(4, redHigh);

    pixelsBack->setPixelColor(0, whiteHigh);
    pixelsBack->setPixelColor(1, red);
    pixelsBack->setPixelColor(2, red);
    pixelsBack->setPixelColor(3, red);
    pixelsBack->setPixelColor(4, whiteHigh);

  } else {
    // standing or very slow
    pixelsFront->setPixelColor(0, whiteUltraLow);
    pixelsFront->setPixelColor(1, whiteLow);
    pixelsFront->setPixelColor(2, whiteLow);
    pixelsFront->setPixelColor(3, whiteLow);
    pixelsFront->setPixelColor(4, whiteUltraLow);

    pixelsBack->setPixelColor(0, redUltraLow);
    pixelsBack->setPixelColor(1, redLow);
    pixelsBack->setPixelColor(2, redLow);
    pixelsBack->setPixelColor(3, redLow);
    pixelsBack->setPixelColor(4, redUltraLow);
  }

  // check steering direction and engage turn signal
  if (rcInput->steerDir == -1) {
    if (blink->blink) {
      pixelsFront->setPixelColor(0, orange);
      pixelsBack->setPixelColor(0, orange);

    } else {
      pixelsFront->setPixelColor(0, black);
      pixelsBack->setPixelColor(0, black);
    }
  } else if (rcInput->steerDir == 1) {
    if (blink->blink) {
      pixelsFront->setPixelColor(4, orange);
      pixelsBack->setPixelColor(4, orange);

    } else {
      pixelsFront->setPixelColor(4, black);
      pixelsBack->setPixelColor(4, black);
    }
  } else {
    // reset blinker
    blink->reset();
  }
}

