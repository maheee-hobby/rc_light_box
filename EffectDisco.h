#ifndef EffectDisco_H
#define EffectDisco_H


#include <Adafruit_NeoPixel.h>

#include "RCInput.h"
#include "Blink.h"


class EffectDisco {

  public:
    EffectDisco();
    void update(RCInput *rcInput, Adafruit_NeoPixel *pixelsFront, Adafruit_NeoPixel *pixelsBack);
};


#endif
