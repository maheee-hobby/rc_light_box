#include <Adafruit_NeoPixel.h>
#include <ESP8266WiFi.h>

#include "RCInput.h"
#include "EffectDrive.h"
#include "EffectDisco.h"
#include "EffectKnightRider.h"
#include "EffectPolice.h"

/*
 * Configuration
 */
#define PIN_INPUT_DIR   D5
#define PIN_INPUT_STEER D6

#define PIN_OUTPUT_FRONT D1
#define PIN_OUTPUT_BACK  D2

#define PIXEL_CNT_FRONT 5
#define PIXEL_CNT_BACK  5


/*
 * Variables
 */
Adafruit_NeoPixel *pixelsFront;
Adafruit_NeoPixel *pixelsBack;

RCInput rcInput(PIN_INPUT_DIR, PIN_INPUT_STEER);

EffectDrive effectDrive;
EffectDisco effectDisco;
EffectKnightRider effectKnightRider;
EffectPolice effectPolice;

int effect = 0;
unsigned long lastActive, inactiveFor;

/*
 * Setup
 */
void setup() {
  WiFi.mode(WIFI_OFF);
  WiFi.forceSleepBegin();
  delay(1);

  pinMode(PIN_INPUT_DIR, INPUT);
  pinMode(PIN_INPUT_STEER, INPUT);

  pinMode(PIN_OUTPUT_FRONT, OUTPUT);
  pinMode(PIN_OUTPUT_BACK, OUTPUT);
  
  //Serial.begin(115200);

  pixelsFront = new Adafruit_NeoPixel(PIXEL_CNT_FRONT, PIN_OUTPUT_FRONT);
  pixelsBack  = new Adafruit_NeoPixel(PIXEL_CNT_BACK,  PIN_OUTPUT_BACK);

  pixelsFront->begin();
  pixelsBack->begin();

  rcInput.update();
  effect = rcInput.steerDir;
}


/*
 * Loop
 */
void loop() {
  rcInput.update();

  if (rcInput.driveDir != 0) {
    lastActive = millis();
  }
  inactiveFor = millis() - lastActive;

  if (effect == 0) {
    if (inactiveFor > 10000) {
      effectKnightRider.update(&rcInput, pixelsFront, pixelsBack);
    } else {
      effectDrive.update(&rcInput, pixelsFront, pixelsBack);
    }
  } else if (effect == -1) {
    effectPolice.update(&rcInput, pixelsFront, pixelsBack);
  } else if (effect == 1) {
    effectKnightRider.update(&rcInput, pixelsFront, pixelsBack);
  }
  //effectDisco.update(&rcInput, pixelsFront, pixelsBack);

  pixelsFront->show();
  pixelsBack->show();

  delay(10);
}
