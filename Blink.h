#ifndef Blink_H
#define Blink_H

#include "Arduino.h"


class Blink {

  private:
    int interval;
    unsigned long lastBlink;

  public:
    bool blink;

    Blink(int interval);
    void update();
    void reset();
};


#endif
