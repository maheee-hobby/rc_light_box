#include "Blink.h"


Blink::Blink(int interval) {
  this->interval = interval;
}

void Blink::update() {
  unsigned long now = millis();
  if (now - lastBlink > interval) {
    blink = !blink;
    lastBlink = now;
  }
}

void Blink::reset() {
    blink = true;
    lastBlink = millis();
}

