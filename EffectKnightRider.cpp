#include "EffectKnightRider.h"


EffectKnightRider::EffectKnightRider() {
  blink = new Blink(200);
  lastBlink = false;
  effectPosition = 0;
}

void EffectKnightRider::update(RCInput *rcInput, Adafruit_NeoPixel *pixelsFront, Adafruit_NeoPixel *pixelsBack) {
  blink->update();

  colorEffect = pixelsFront->Color(255, 0, 0);
  colorBlack  = pixelsFront->Color(0, 0, 0);

  if (lastBlink != blink->blink) {
    lastBlink = blink->blink;

    ++effectPosition;
    if (effectPosition >= 8) {
      effectPosition = 0;
    }
  }

  int activeLedNo = effectPosition % 9;
  if (activeLedNo > 4) {
    activeLedNo = 8 - activeLedNo;
  }

  pixelsFront->setPixelColor(0, colorBlack);
  pixelsFront->setPixelColor(1, colorBlack);
  pixelsFront->setPixelColor(2, colorBlack);
  pixelsFront->setPixelColor(3, colorBlack);
  pixelsFront->setPixelColor(4, colorBlack);
  
  pixelsBack->setPixelColor(0, colorBlack);
  pixelsBack->setPixelColor(1, colorBlack);
  pixelsBack->setPixelColor(2, colorBlack);
  pixelsBack->setPixelColor(3, colorBlack);
  pixelsBack->setPixelColor(4, colorBlack);

  pixelsFront->setPixelColor(activeLedNo, colorEffect);
  pixelsBack->setPixelColor(activeLedNo, colorEffect);
}

