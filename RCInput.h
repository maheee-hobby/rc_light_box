#ifndef RCInput_H
#define RCInput_H

#include "Arduino.h"


#define MIN_INPUT_VALUE      1000
#define MAX_INPUT_VALUE      2000
#define STEER_LOW_THRESHOLD  1300
#define STEER_HIGH_THRESHOLD 1400
#define DIR_LOW_THRESHOLD    1460
#define DIR_HIGH_THRESHOLD   1540


class RCInput {

  private:
    uint8_t pinInputDir;
    uint8_t pinInputSteer;

    int driveInputValue;
    int steerInputValue;
    int newDriveInputValue;
    int newSteerInputValue;

    int readPwm(uint8_t pin);

  public:
    int driveDir;
    int driveVal;
    int steerDir;
    int steerVal;

    RCInput(uint8_t pinInputDir, uint8_t pinInputSteer);
    void update();
};


#endif
