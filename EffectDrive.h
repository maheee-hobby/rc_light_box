#ifndef EffectDrive_H
#define EffectDrive_H


#include <Adafruit_NeoPixel.h>

#include "RCInput.h"
#include "Blink.h"


#define BLINK_INTERVAL 666


class EffectDrive {

  private:
    Blink *blink;

    uint32_t whiteUltraLow;
    uint32_t whiteLow;
    uint32_t white;
    uint32_t whiteHigh;
    uint32_t redUltraLow;
    uint32_t redLow;
    uint32_t red;
    uint32_t redHigh;
    uint32_t orange;
    uint32_t black;

  public:
    EffectDrive();
    void update(RCInput *rcInput, Adafruit_NeoPixel *pixelsFront, Adafruit_NeoPixel *pixelsBack);
};


#endif
