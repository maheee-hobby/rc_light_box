#ifndef math_H
#define math_H


int restrict(int value, int min, int max) {
  if (value < min) return min;
  if (value > max) return max;
  return value;
}


#endif
