#ifndef EffectKnightRider_H
#define EffectKnightRider_H


#include <Adafruit_NeoPixel.h>

#include "RCInput.h"
#include "Blink.h"


class EffectKnightRider {

  private:
    Blink *blink;
    bool lastBlink;

    int effectPosition;

    uint32_t colorEffect;
    uint32_t colorBlack;

  public:
    EffectKnightRider();
    void update(RCInput *rcInput, Adafruit_NeoPixel *pixelsFront, Adafruit_NeoPixel *pixelsBack);
};


#endif
