#include "EffectPolice.h"


EffectPolice::EffectPolice() {
  blink = new Blink(600);
  lastBlink = false;
  effectPosition = 0;
  flash = 0;
}

void EffectPolice::update(RCInput *rcInput, Adafruit_NeoPixel *pixelsFront, Adafruit_NeoPixel *pixelsBack) {
  blink->update();
  flash = 1 - flash;

  colorRed    = pixelsFront->Color(255, 0, 0);
  colorBlue   = pixelsFront->Color(0, 0, 255);
  colorBlack  = pixelsFront->Color(0, 0, 0);

  if (lastBlink != blink->blink) {
    lastBlink = blink->blink;

    ++effectPosition;
    if (effectPosition >= 4) {
      effectPosition = 0;
    }
  }

  pixelsFront->setPixelColor(0, colorBlack);
  pixelsFront->setPixelColor(1, colorBlack);
  pixelsFront->setPixelColor(2, colorBlack);
  pixelsFront->setPixelColor(3, colorBlack);
  pixelsFront->setPixelColor(4, colorBlack);
  
  pixelsBack->setPixelColor(0, colorBlack);
  pixelsBack->setPixelColor(1, colorBlack);
  pixelsBack->setPixelColor(2, colorBlack);
  pixelsBack->setPixelColor(3, colorBlack);
  pixelsBack->setPixelColor(4, colorBlack);

  if (flash) {
    switch (effectPosition) {
      case 0:
        pixelsFront->setPixelColor(0, colorRed);
        pixelsFront->setPixelColor(3, colorBlue);
        pixelsBack->setPixelColor(1, colorRed);
        pixelsBack->setPixelColor(4, colorBlue);
        break;
      case 1:
        pixelsFront->setPixelColor(1, colorRed);
        pixelsFront->setPixelColor(4, colorBlue);
        pixelsBack->setPixelColor(0, colorRed);
        pixelsBack->setPixelColor(3, colorBlue);
        break;
      case 2:
        pixelsFront->setPixelColor(0, colorBlue);
        pixelsFront->setPixelColor(3, colorRed);
        pixelsBack->setPixelColor(1, colorBlue);
        pixelsBack->setPixelColor(4, colorRed);
        break;
      case 3:
        pixelsFront->setPixelColor(1, colorBlue);
        pixelsFront->setPixelColor(4, colorRed);
        pixelsBack->setPixelColor(0, colorBlue);
        pixelsBack->setPixelColor(3, colorRed);
        break;
    }
  }

}
