#include "EffectDisco.h"


EffectDisco::EffectDisco() {
}

void EffectDisco::update(RCInput *rcInput, Adafruit_NeoPixel *pixelsFront, Adafruit_NeoPixel *pixelsBack) {
  uint32_t color = pixelsFront->Color(random(0,255), random(0,255), random(0,255));

  pixelsFront->setPixelColor(0, color);
  pixelsFront->setPixelColor(1, color);
  pixelsFront->setPixelColor(2, color);
  pixelsFront->setPixelColor(3, color);
  pixelsFront->setPixelColor(4, color);
  
  pixelsBack->setPixelColor(0, color);
  pixelsBack->setPixelColor(1, color);
  pixelsBack->setPixelColor(2, color);
  pixelsBack->setPixelColor(3, color);
  pixelsBack->setPixelColor(4, color);
}

